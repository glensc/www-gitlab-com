---
layout: job_page
title: "Content Designer"
---
As the content designer for GitLab you will be responsible for assisting the marketing team on create engaging visual content across all channels. Projects will range from promotional videos, social campaigns, and infographics, to ebooks, whitepapers, and illustrations. Our ideal candidate has a strong design skill-set, takes direction openly and positively, and is very detail oriented. Major bonus points if you’ve used GitLab before!   

## Responsibilities  

* You create a great design for interesting content.   
* Work with our content team on video creation and editing.  
* GIF creation for social campaigns and email.   
* Social media campaign assets to support our content marketing.   
* Design demand generation content like eBooks and whitepapers working directly with our content marketing manager.   
* You’ve created super cool infographics or graphics to tell a story based on data or interesting idea.    
* Illustrations for documentation like flow charts and graphical representations of complex development practices.   
* Create short, punchy videos to explain our awesome features. If we give you a voice-over, you can create a great quick video.   

## Requirements
* Excellent spoken and written English.  
* Self taught or degrees welcome.   
* 2-3 years experience in marketing-focused design is preferred.  
* Able to iterate quickly from concept and mockups to pixel-perfect design.  
* Comfortable working very collaboratively on projects, as well as independently.   
* Be ready to learn how to use GitLab and Git.
* You share our values, and work in accordance with those values.  

## Apply

Please note that if we are actively hiring for a position, you will see it listed on our [jobs page](https://about.gitlab.com/jobs/), where all of our current openings are advertised. To apply, please click on the name of the role you are interested in, which will take you to our applicant tracking system (ATS), [Lever](https://www.lever.co/).

Avoid the [confidence gap](https://www.theatlantic.com/magazine/archive/2014/05/the-confidence-gap/359815/
); you do not have to match all the listed requirements exactly to apply. Our hiring process is described in more detail in our [hiring handbook](https://about.gitlab.com/handbook/hiring/).

## About GitLab

GitLab Inc. is a company based on the GitLab open-source project. GitLab is a community project to which over 1,000 people worldwide have contributed. We are an active participant in this community, trying to serve its needs and lead by example. We have one [vision](https://about.gitlab.com/strategy/): everyone can contribute to all digital content, and our mission is to change all creative work from read-only to read-write so that everyone can contribute.

We [value](https://about.gitlab.com/handbook/values/) results, transparency, sharing, freedom, efficiency, frugality, collaboration, directness, kindness, diversity, boring solutions, and quirkiness. If these values match your personality, work ethic, and personal goals, we encourage you to visit our [primer](https://about.gitlab.com/primer/) to learn more. Open source is our culture, our way of life, our story, and what makes us truly unique.

Top 10 reasons to work for GitLab:

1. Work with helpful, kind, motivated, and talented people.
1. Work remote so you have no commute and are free to travel and move.
1. Have flexible work hours so you are there for other people and free to plan the day how you like.
1. Everyone works remote, but you don't feel remote. We don't have a head office, so you're not in a satellite office.
1. Work on open source software so you can interact with a large community and can show your work.
1. Work on a product you use every day: we drink our own wine.
1. Work on a product used by lots of people that care about what you do.
1. As a company we contribute more than we take, most of our work is released as the open source GitLab CE.
1. Focused on results, not on long hours, so that you can have a life and don't burn out.
1. Open internal processes: know what you're getting in to and be assured we're thoughtful and effective.

See [our culture page](https://about.gitlab.com/culture) for more!

Work remotely from anywhere in the world. Curious to see what that looks like? Check out our [remote manifesto](https://about.gitlab.com/2015/04/08/the-remote-manifesto/).

