---
layout: markdown_page
title: "GitLab Lyra (India) Benefits"
---

Can't find what you're looking for? Try the main [People Operations page](/handbook/people-operations).

## On this page
{:.no_toc}

- TOC
{:toc}

----

## Specific to India based employees

All of the benefits listed below are administered and managed by [Lyra Infosystems](http://lyrainfo.com/). As part of the onboarding process Lyra will reach out to team members in their first week to arrange setup and enrollment.  Should you have any questions about these benefits please contact hr@lyrainfo.com or the People Operations Team at GitLab who are also very happy to assist.

### Medical Benefits

 - Group Mediclaim Policy which will take care of hospitalization expenses, the coverage provided is available for the team member, their spouse and up to two children.  Should additional cover be required this will need to be purchased by the team member in the form of an Individual Policy. This can not be purchased under the Group Mediclaim Policy.
 - Group Personal Accident policy including accidental death benefit.

### Meal Vouchers

Zeta Meal Cards are an optional employee purchased benefit. These Meal Cards work like a Debit Card, which can be used at any outlet selling food items and non-alcoholic beverages that accept card payments. If you would like purchase these a deduction from salary will be made each month.

### Other LYRA Benefits

LYRA is also able to provide financial assistance to employees, where employees can obtain a special rate of interests for all their loan requirements from HDFC bank.
